<?php
function custom_menu( $menu_name,  $classMenu = '', $classSubMenu = '', $aClass = ''){
  $menu = wp_get_nav_menu_object( $menu_name );
  $menuitems = wp_get_nav_menu_items( $menu->term_id);

  $menu_list = '';
  $count = 0;
  $submenu = false;
  if($menuitems)
  foreach( $menuitems as $item ):

    $link = $item->url;
    $title = $item->title;
    // item does not have a parent so menu_item_parent equals 0 (false)
    if ( !$item->menu_item_parent ):
      $parent_id = $item->ID;

      $menu_list .= '
        <li class="' . $classMenu . '">
          <a href="'.$link.'">
              '.$title.'
          </a>';
    endif; 

    if ( $parent_id == $item->menu_item_parent ): 

      if ( !$submenu ): $submenu = true;
        $menu_list .= '<ul class="sub-menu">';
      endif;

      $menu_list .= 
        '<li class="' . $classSubMenu . '">
          <a href="'.$link.'" class="' . $aClass . '">'.$title.'</a>
        </li>';

      if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ):
        $menu_list .= '</ul>';
        $submenu = false; 
      endif;
    endif;

    if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ):
      $menu_list .= '</li>';                         
      $submenu = false; 
    endif;

    $count++; 
  endforeach;

  echo $menu_list;
}

  
?>