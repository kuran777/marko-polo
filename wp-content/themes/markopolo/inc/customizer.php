<?php
function mytheme_customize_register( $wp_customize ) {

$wp_customize->add_section(
    // ID
    'data_header_section',
    // Arguments array
    array(
        'title' => 'Дополнительные данные',
        'capability' => 'edit_theme_options',
        'description' => "Тут можно указать дополнительные данные сайта"
    )
);

$wp_customize->add_setting(
    // ID
    'site_o_email',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_email',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "email",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_email'
    )
);


$wp_customize->add_setting(
    // ID
    'site_o_address',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_address',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Адрес",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_address'
    )
);

$wp_customize->add_setting(
    // ID
    'site_o_copyright',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_copyright',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Copyright",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_copyright'
    )
);

$wp_customize->add_setting(
    // ID
    'site_o_vk',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_vk',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Вконтакте ссылка",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_vk'
    )
);

$wp_customize->add_setting(
    // ID
    'site_o_f',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_f',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Facebook ссылка",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_f'
    )
);

$wp_customize->add_setting(
    // ID
    'site_o_insta',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_insta',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Instagram ссылка",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_insta'
    )
);

$wp_customize->add_setting(
    // ID
    'site_o_ok',
    // Arguments array
    array(
        'default' => '',
        'type' => 'option'
    )
);
$wp_customize->add_control(
    // ID
    'site_c_ok',
    // Arguments array
    array(
        'type' => 'text',
        'label' => "Одноклассники ссылка",
        'section' => 'data_header_section',
        // This last one must match setting ID from above
        'settings' => 'site_o_ok'
    )
);

$wp_customize->add_setting('site_logo');
$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'site_logo',
        array(
            'label' => 'Логотип сайта',
            'section' => 'title_tagline',
            'settings' => 'site_logo'
        )
    )
);
}
add_action( 'customize_register', 'mytheme_customize_register' );
?>