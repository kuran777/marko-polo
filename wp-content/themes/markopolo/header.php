<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" id="fly-popup-css" href="<?=get_template_directory_uri();?>/css/fly-popup.css" type="text/css" media="all">
    <link rel="stylesheet" id="fly-top-bar-css" href="<?=get_template_directory_uri();?>/css/fly-top-bar.css" type="text/css" media="all">
    <link rel="stylesheet" id="form-grid-css" href="<?=get_template_directory_uri();?>/css/form-grid.css" type="text/css" media="all">
    <link rel="stylesheet" id="selectize-css" href="<?=get_template_directory_uri();?>/css/selectize.css" type="text/css" media="all">
    <link rel="stylesheet" id="datetimepicker-css" href="<?=get_template_directory_uri();?>/css/jquery.datetimepicker.css" type="text/css" media="all">
    <link rel="stylesheet" id="animate-css" href="<?=get_template_directory_uri();?>/css/animate.css" type="text/css" media="all">
    <link rel="stylesheet" id="prettyPhoto-css" href="<?=get_template_directory_uri();?>/css/prettyPhoto.css" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-css" href="<?=get_template_directory_uri();?>/css/font-awesome.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="fw-mmenu-css" href="<?=get_template_directory_uri();?>/css/jquery.mmenu.all.css" type="text/css" media="all">
    <link rel="stylesheet" id="bootstrap-css" href="<?=get_template_directory_uri();?>/css/bootstrap.css" type="text/css" media="all">
    <link rel="stylesheet" id="fw-style-css" href="<?=get_template_directory_uri();?>/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="fw-ext-builder-frontend-grid-css" href="<?=get_template_directory_uri();?>/css/frontend-grid.css" type="text/css" media="all">

    <script async="" src="<?=get_template_directory_uri();?>/js/gtm.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/modernizr.min.js"></script>
    
    <link href="<?=get_template_directory_uri();?>/css/css" rel="stylesheet" type="text/css">
    <link href="<?=get_template_directory_uri();?>/css/header.css" rel="stylesheet" type="text/css">
    <?php wp_head(); ?>
    </head>
</head>
<body class="page-template page-template-visual-builder-template page-template-visual-builder-template-php page has-fly-buy-bar">
    <!--/ Loading Spinner -->
	<div id="page" class="hfeed site animated fadeIn">
		<header class="fly-header-site fly-header-type-1 fly-sticky-header-on sticky-menu sticky-open" style="top: -187px;">
			<div class="fly-nav-wrap fly-nav-left">
				<nav id="fly-menu-primary" class="fly-site-navigation primary-navigation">
				<?php
					$lang = pll_current_language();
					wp_nav_menu( array(
							'menu' => 'Left menu_'.$lang,
							'menu_class'=>'fly-nav-menu',
							'theme_location'=>'markopolo',
							'after'=>''
					) );
				?>
				</nav>				
			</div>
			<!--Logo-->
			<div class="fly-wrap-logo">
				<a class="fly-logo" href="<?= home_url(); ?>" hidefocus="true" style="outline: none;">
						<img src="<?=get_theme_mod('site_logo');?>" alt="Logo">
				</a>
			</div>
			<div class="fly-nav-wrap fly-nav-right">
				<nav id="fly-menu-secondary" class="fly-site-navigation">
				<?php
					$lang = pll_current_language();
					wp_nav_menu( array(
							'menu' => 'Right menu_'.$lang,
							'menu_class'=>'fly-nav-menu',
							'theme_location'=>'markopolo',
							'after'=>''
					) );
				?>
				</nav>
			</div>
		</header>