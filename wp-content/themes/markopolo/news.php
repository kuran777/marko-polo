<?php /*
Template Name: News
Template Post Type: page
 */
 ?>
<?php get_header(); ?>
<div id="main" class="site-main" role="main">
    <section class="fly-section-image fly-header-image fly-section-overlay fly-section-height-md parallax" style="background-image: url(&quot;<?php the_post_thumbnail_url(); ?>&quot;); background-position: 50% -59px;">
        <div class="container">
            <div class="row">
                <h3 class="fly-section-image-title-before"><?php the_field('title_before');?></h3>
                <h2 class="fly-section-image-title-after"><? the_title(); ?></h2>
            </div>
        </div>
    </section>
<section class="fly-main-row fly-sidebar-right">
<div class="fly-divider-space space-sm"></div>
<div class="container">
    <div class="row">
        <div class="fly-content-area col-md-12 col-sm-12">
            <div class="fly-col-inner">
                <div class="post-list">
                    <?php
                    $params = array(
                    'cat'      => get_field('news_category'),
                    'posts_per_page' => 0,
                    'post_type'       => 'post'
                    );
                    query_posts($params);
                    $wp_query->is_archive = false;
                    $wp_query->is_home = false;
                    $index = 1;
                    while(have_posts()): the_post(); 
                    
                    ?>
                    <article id="post-26" class="post clearfix <?=($index%2)?'post-thumbnail-left':'post-thumbnail-right';?>  type-post status-publish format-standard has-post-thumbnail hentry category-press category-recipes tag-bar tag-bistro tag-food tag-ingridients tag-restaurant">
                        <div class="fly-post-image">
                            <a class="fly-post-thumbnail" href="<?= get_permalink( get_the_ID() ) ?>" hidefocus="true" style="outline: none;">
                                <img src="<?= get_the_post_thumbnail_url( get_the_ID(), 'newsImg' ) ?>" alt="">			</a>
                        </div>
                        <div class="fly-post-content">
                            <header class="entry-header">

                                <div class="entry-meta">
                                    <a class="entry-date" href="<?= get_permalink( get_the_ID() ) ?>" rel="bookmark" hidefocus="true" style="outline: none;">
                                        <time class="fly-post-date"><?= the_field( 'news_date', get_the_ID() ); ?></time>
                                    </a>
                                </div>
                                <h2 class="entry-title"><a href="<?= get_permalink( get_the_ID() ) ?>" hidefocus="true" style="outline: none;"><?php the_title(); ?></a></h2>
                            </header>
                            <div class="entry-content">
                                <?php the_excerpt();?>
                            </div>
                        </div>
                    </article>
                    <?php $index ++; endwhile; ?>
                </div><!-- /.postlist-->
            </div>
        </div><!-- /.content-area-->


    </div><!-- /.row-->
</div><!-- /.container-->
<div class="fly-divider-space space-archive"></div>
</section>
</div><!--#main-->
<?php get_footer(); ?>