<?php /*
Template Name: Menu
Template Post Type: page
 */
 ?>
<?php get_header(); ?>
<div id="main" class="site-main" role="main"><div class="fw-page-builder-content"><section class="fw-main-row  5787513408a60763ceb1cf70654acd68 fly-section-height-auto">
            <div class="fw-container-fluid">
                <div class="fw-row">
                  <div class="fw-col-xs-12">
                    <section class="fly-section-image fly-header-image parallax fly-section-overlay fly-section-height-md 2ff1d2151e7b3b7e3fdb22a2153d22a4 header-image-shortcode" style="background-image: url(&quot;<?php the_post_thumbnail_url(); ?>&quot;); background-position: 50% -41px;">
                      <div class="container">
                        <div class="row">
                          <h3 class="fly-section-image-title-before"><?php the_field('title_before');?></h3>
                          <h2 class="fly-section-image-title-after"><? the_title(); ?></h2>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>

                <div class="fw-row">
                  <div class="fw-col-xs-12">
                    <div class="fly-divider-space space-sm"></div>
                  </div>
                </div>

                <?php if( have_rows('menu_box') ): ?>
                    <?php while( have_rows('menu_box') ): the_row(); ?>
                    <div class="fw-row">
                      <div class="fw-col-xs-12">
                        <section class="fly-dish-menu container-min ">
                            <div class="container">
                                <div class="row">
                                  <h3 class="fly-dish-menu-title"><?php the_sub_field('category_name'); ?></h3>
                                  <div class="fly-menu-title-separator"></div>
                                  <ul class="fly-dish-menu-content">
                                    <?php if( have_rows('menu_lists') ): ?>
                                      <?php while( have_rows('menu_lists') ): the_row(); ?>
                                        <li>
                                          <div class="fly-dish-menu-description">
                                            <div class="fly-dish-image">
                                                <img src="<?php the_sub_field('image'); ?>" alt="">
                                            </div>
                                            <h5><?php the_sub_field('name'); ?></h5>
                                            <?php the_sub_field('сomposition'); ?>													
                                          </div>
                                          <div class="fly-dish-menu-price">$<?php the_sub_field('price'); ?></div>
                                        </li>
                                      <?php endwhile; ?>
                                    <?php endif; ?>
                                  </ul>
                                </div>
                            </div>
                        </section></div>
                      </div>
                      <div class="fw-row">
                        <div class="fw-col-xs-12">
                          <div class="fly-divider-space space-sm"></div>
                        </div>
                      </div> 
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div><!--#main-->

<?php get_footer(); ?>