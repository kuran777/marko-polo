<?php 

if( ! is_admin() ){
	add_action('wp_enqueue_scripts', 'jquery_enqueue_func', 11);
	function jquery_enqueue_func(){
		wp_deregister_script('jquery');
		wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js", false, null);
		wp_enqueue_script('jquery');

		/*wp_register_script('my-common', get_template_directory_uri()."/js/common.js", false, null);
		wp_enqueue_script('my-common');


		wp_register_script('infiniti', 'https://unpkg.com/infinite-scroll@3.0.0/dist/infinite-scroll.pkgd.min.js', false, null );
		wp_enqueue_script('infiniti');*/
	}
}


function IEhtml5_shim_func(){
	echo '<!--[if lt IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->';
	echo '<!--[if lt IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script><![endif]-->';
}
add_action('wp_head', 'IEhtml5_shim_func');


remove_action('wp_head', 'wp_generator');
add_filter('the_generator', '__return_empty_string');

//define('DISALLOW_FILE_EDIT', true);


if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails', array( 'page', 'post' ));
}

if( function_exists('add_image_size') ){
	add_image_size('menuImg', 80, 80, true);
	add_image_size('newsImg', 360, 320, true);
}

register_nav_menus(array(
	'main_left'    => 'Main left menu',
	'main_right'    => 'Main right menu'
));

add_action( 'current_screen', 'my_theme_add_editor_styles' );
function my_theme_add_editor_styles() {
	add_editor_style( 'editor-styles.css' );
}


//remove_filter( 'the_content', 'wpautop' );
//remove_filter( 'the_excerpt', 'wpautop' );

show_admin_bar(false);

require get_parent_theme_file_path( '/inc/customizer.php' );
require get_parent_theme_file_path( '/inc/custom_nav.php' );