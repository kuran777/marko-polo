<?php /*
Template Name: Contact
Template Post Type: page
 */
 ?>
<?php get_header();?>
<div id="main" class="site-main" role="main"><div class="fw-page-builder-content"><section  class="fw-main-row  735c38aed188c290c0330ab418d4dd75 fly-section-height-auto"  >
		<div class="fw-container-fluid">
		<div class="fw-row">
	<div class="fw-col-xs-12">
	<section class="fly-section-image fly-header-image  fly-section-overlay fly-section-height-md bb5e11a426232ed97b158b991b97cdf9 header-image-shortcode" style="background-image:url(<?php the_post_thumbnail_url(); ?>);" >
        <div class="container">
		<div class="row">
							<h3 class="fly-section-image-title-before"><?= the_field('title_before');?></h3>
										<h2 class="fly-section-image-title-after"><?= the_title();?></h2>
					</div>
	</div>
</section></div>
</div>
<?php
if (have_posts()):while (have_posts()):the_post(); 
    the_content(); 
endwhile; else:
    __('Извините такой страницы не найдено!'); 
endif;
?>
	</div><!--#main-->
    <?php get_footer();?>