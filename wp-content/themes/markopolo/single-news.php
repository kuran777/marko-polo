<?php /*
Template Name: Single news
Template Post Type: post
 */
 ?>
<?php get_header(); ?>
<div id="main" class="site-main" role="main">			<section class="fly-section-image fly-header-image fly-section-overlay fly-section-height-md parallax" style="background-image: url(&quot;<?php the_post_thumbnail_url(); ?>&quot;); background-position: 50% -59px;">
<div class="container">
    <div class="row">
        <h3 class="fly-section-image-title-before"><?php the_field('title_before');?></h3>
        <h2 class="fly-section-image-title-after"><? the_title(); ?></h2>
    </div>
</div>
</section>
<section class="fly-main-row fly-sidebar-right">
<div class="fly-divider-space space-sm"></div>
<div class="container">
    <div class="row">
        <div class="fly-content-area col-md-12 col-sm-12">
            <div class="fly-col-inner">
              <?php
              if (have_posts()):while (have_posts()):the_post(); ?>
              <article class="post post-details clearfix" xmlns="http://www.w3.org/1999/html">
                <header class="entry-header">
                  <div class="entry-meta">
                        <a class="entry-date" href="http://demo.flytemplates.com/flycoffee-wp/when-this-way/" rel="bookmark" hidefocus="true" style="outline: none;">
                        <time class="fly-post-date"><?= the_field('news_date');?></time>
                      </a>
                        </div>
                  <h2 class="entry-title"><?= the_title(); ?></h2>
                </header>
                <div class="fly-post-image">
                  <img width="700" height="360" src="<?php the_post_thumbnail_url(); ?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" style="margin: 0 auto; display: block;">		
                </div>
                <div class="entry-content">
                  <?= the_content(); ?>
                </div>
              </article>
              <?php  endwhile; else:
                  __('Извините такой страницы не найдено!'); 
                endif;
              ?>
              <div class="fly-post-details-meta">                
                <div class="fly-post-details-back-to-list-btn"><a class="fly-btn fly-btn-1 fly-btn-md fly-btn-color-2" href="javascript:history.go(-1)" hidefocus="true" style="outline: none;"><span>BACK TO LIST</span></a></div>
              </div>
            </div>
        </div><!-- /.content-area-->


    </div><!-- /.row-->
</div><!-- /.container-->
<div class="fly-divider-space space-archive"></div>
</section>
</div><!--#main-->
<?php get_footer(); ?>