<?php /*
Template Name: About
Template Post Type: page
 */
 ?>

<?php get_header();?>
<div id="main" class="site-main" role="main"><div class="fw-page-builder-content">
    <section class="fw-main-row  953c4793813f81162ccb35c28b7257d3 fly-section-height-auto">
                        <div class="fw-container-fluid">
                            <div class="fw-row">
                                <div class="fw-col-xs-12">
                                    <section class="fly-section-image fly-header-image  fly-section-overlay fly-section-height-md 7558dab8ea04e23e5dd88d25dda67944 header-image-shortcode" style="background-image:url(<?php the_post_thumbnail_url(); ?>);">
                                        <div class="container">
                                            <div class="row">
                                                <h3 class="fly-section-image-title-before"><?= the_field('title_before');?></h3>
                                                <h2 class="fly-section-image-title-after"><?= the_title();?></h2>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
<?php
if (have_posts()):while (have_posts()):the_post(); 
    the_content(); 
endwhile; else:
    __('Извините такой страницы не найдено!'); 
endif;
?>
</div>
</section>
</div><!--#main-->
<?php get_footer();?>