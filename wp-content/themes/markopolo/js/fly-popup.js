'use strict';

jQuery(function($) {
	$(window).on('load', function() {
		var popup = $('.fly-buy-popup'),
			toggle = popup.find('.fly-buy-popup-toggle');

		if(!localStorage.getItem('isFTVisited')) {
			localStorage.setItem('isFTVisited', true);

			setTimeout(function() {
				popup.addClass('active');
			}, 10000);
		}

		toggle.on('click', function() {
			popup.toggleClass('active');
		});
	});
});