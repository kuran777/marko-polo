'use strict';

jQuery(function($) {
	var $body = $('body'),
		bar = $('.fly-buy-bar'),
		toggle = bar.find('.fly-buy-bar-toggle');

	$body.addClass('has-fly-buy-bar');

	toggle.on('click', function() {
		bar.toggleClass('active');
		$body.toggleClass('has-fly-buy-bar-closed');
	});
});