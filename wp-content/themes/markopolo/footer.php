    <!-- Footer -->
    <footer class="fly-site-footer">
      <div class="container">
        <div class="row">
            <div class="fly-divider-space space-sm"></div>
        </div>
        <div class="row">
          <div class="fly-footer-content clearfix">
              <aside id="mc4wp_form_widget-2" class="widget widget_mc4wp_form_widget">
                <h2 class="widget-title">
                  <span>SUBSCRIBE TO NEWSLETTER</span>
                </h2>
                <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-258 mc4wp-form-basic" method="post" data-id="258" data-name="Default sign-up form">
                  <div class="mc4wp-form-fields">
                    <p>
                      <label>Email address: </label>
                      <input type="email" id="mc4wp_email" name="EMAIL" placeholder="Your email address" required="" hidefocus="true" style="outline: none;">
                    </p>

                    <p>
                      <input type="submit" value="Sign up" hidefocus="true" style="outline: none;">
                    </p>
                    <div style="display: none;">
                      <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" hidefocus="true" style="outline: none;">
                    </div>
                    <input type="hidden" name="_mc4wp_timestamp" value="1507631368" hidefocus="true" style="outline: none;">
                    <input type="hidden" name="_mc4wp_form_id" value="258" hidefocus="true" style="outline: none;">
                    <input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" hidefocus="true" style="outline: none;">
                  </div>
                  <div class="mc4wp-response"></div>
                </form><!-- / MailChimp for WordPress Plugin -->
              </aside>								
              <a class="fly-footer-logo" href="http://demo.flytemplates.com/flycoffee-wp/menu/" hidefocus="true" style="outline: none;">
                <img src="<?=get_theme_mod('site_logo');?>" alt="Logo">
              </a>
              <div class="fly-footer-info">bar, bistro &amp; restaurant <br> 
                41126 Oxford Road - england - phone <a href="callto:44 023 642 124" hidefocus="true" style="outline: none;">+44 023 642 124</a>
              </div>
              <div class="fly-social"><a class="fly-social-link" target="_blank" href="https://www.facebook.com/flytemplates" hidefocus="true" style="outline: none;"><i class="fa fa-facebook"></i></a>
                  <a class="fly-social-link" target="_blank" href="https://twitter.com/FlyTemplates" hidefocus="true" style="outline: none;"><i class="fa fa-twitter"></i></a>
                  <a class="fly-social-link" target="_blank" href="https://plus.google.com/u/1/104088102961366190029/posts" hidefocus="true" style="outline: none;"><i class="fa fa-google-plus"></i></a>
                  <a class="fly-social-link" target="_blank" href="https://instagram.com/flytemplates" hidefocus="true" style="outline: none;"><i class="fa fa-instagram"></i></a>
              </div>				
            </div>
        </div>
        <div class="row">
          <div class="fly-divider-space space-sm"></div>
        </div>
      </div>
      <div class="fly-footer-copyright">
        <div class="container">
          <div class="row">
            <div class="fly-copyright-text">
              Restaurant WordPress Theme © made by 
              <a rel="nofollow" href="http://flytemplates.com/" target="_blank" hidefocus="true" style="outline: none;">Flytemplates</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div><!-- /#page -->

  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/fly-popup.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/fly-top-bar.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.blockUI.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.datetimepicker.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/comment-reply.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.touchSwipe.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.prettyPhoto.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.customInput.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/selectize.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/scrollTo.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.parallax-1.1.3.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/jquery.mmenu.min.all.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/masonry.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/general.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/html5shiv.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/respond.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/wp-embed.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/js/froogaloop2.min.js"></script>
  <!--[if lte IE 9]>
  <script type='text/javascript' src='http://demo.flytemplates.com/flycoffee-wp/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js'></script>
  <![endif]-->
  <?php wp_footer(); ?>
</body>
</html>