<?php /*
Template Name: Gallery
Template Post Type: page
 */
?>

<?php get_header(); ?>
<div id="main" class="site-main" role="main">			<section class="fly-section-image fly-header-image fly-section-overlay fly-section-height-md parallax" style="background-image: url(&quot;<?php the_post_thumbnail_url(); ?>&quot;); background-position: 50% -25px;">
<div class="container">
    <div class="row">
        <h2 class="fly-section-image-title-after">Gallery</h2>
    </div>
</div>
</section>
<div class="fly-divider-space space-sm" style="height: 40px;"></div>
<section class="fly-main-row fly-wrap-photo-gallery">
<div class="container">
    <div class="row">
      <?php
      if (have_posts()):while (have_posts()):the_post(); 
          the_content(); 
      endwhile; else:
          __('Извините такой страницы не найдено!'); 
      endif;
      ?>
    </div>  
    <div class="row">
        <!--Content Area-->
        <div class="fly-photo-gallery col-sm-12">
            <div class="fly-photo-list-item">

              <?php
                if (get_field('gallery_images')){
                  foreach(get_field('gallery_images') as $imageArr){ ?>
                    <a href="<?= $imageArr['url']; ?>"  " data-rel="prettyPhoto[1]" rel="prettyPhoto[1]" class="photo photo-width1 active" hidefocus="true">
                      <img src="<?= $imageArr['url']; ?>" alt="">
                      <div class="fly-photo-list-overlay">
                        <div class="fly-itable">
                          <div class="fly-icell"><i class="flyicon-expand"></i></div>
                        </div>
                      </div>
                    </a>
              <?php }
                }
              ?>
            </div>
        </div><!-- /fly-photo-gallery -->
    </div><!-- /.row -->
</div><!-- /.container -->
</section>
<div class="fly-divider-space space-sm"></div>
</div><!--#main-->
<?php get_footer(); ?>