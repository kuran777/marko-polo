<?php /*
Template Name: Home
Template Post Type: page
 */
 ?>
<?php get_header(); ?>
<div id="main" class="site-main" role="main">
    <div class="fw-page-builder-content">
        <section class="fw-main-row  2148eecd91a622cd3127d60c4afb8a6c fly-section-height-auto">
            <div class="fw-container-fluid">
                <div class="fw-row">
                    <div class="fw-col-xs-12">
                        <script>
                            var tag = document.createElement('script');
                            tag.src = "https://www.youtube.com/iframe_api";
                            var firstScriptTag = document.getElementsByTagName('script')[0];
                            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                        </script>

                        <section class="fly-slider-full">
                            <div class="main-carousel default">
                                <div id="Carousel-929de0353e9e4d64bc7f57bd1cf391e2" class="carousel slide">
                                    <div class="carousel-inner">
                                        <!-- Carousel items -->
                                        <div class="item parallax active" style="background-image: url(&quot;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/slide-1.jpg&quot;); background-position: 50% -35px; height: 720px;">

                                            <div class="container">
                                                <div class="fly-itable">
                                                    <div class="fly-icell">
                                                        <div class="fly-wrap-text-slider">
                                                            <h1 class="fly-slider-title-before">RESTAURANT&amp; EVENTS<br>IN THE BORDERS</h1>
                                                            <div class="fly-slider-divider"></div>
                                                            <h2 class="fly-slider-title-after">Delicious, beverages and great-tasting food.<br>The secret to making life better.</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Carousel items -->
                                        <!-- Carousel items -->
                                        <div class="item parallax " style="background-image: url(&quot;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/slide-2.jpg&quot;); background-position: 50% -35px; height: 720px;">

                                            <div class="container">
                                                <div class="fly-itable">
                                                    <div class="fly-icell">
                                                        <div class="fly-wrap-text-slider">
                                                            <h1 class="fly-slider-title-before">GATHER WITH FAMILY &amp;<br>GREAT FOOD</h1>
                                                            <div class="fly-slider-divider"></div>
                                                            <h2 class="fly-slider-title-after">Delicious, beverages and great-tasting food.<br>The secret to making life better.</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Carousel items -->
                                        <!-- Carousel items -->
                                        <div class="item parallax " style="background-image: url(&quot;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/slide-3.jpg&quot;); background-position: 50% -35px; height: 720px;">

                                            <div class="container">
                                                <div class="fly-itable">
                                                    <div class="fly-icell">
                                                        <div class="fly-wrap-text-slider">
                                                            <h1 class="fly-slider-title-before">LOCALLY SOURCED<br>PRODUCE</h1>
                                                            <div class="fly-slider-divider"></div>
                                                            <h2 class="fly-slider-title-after">Delicious, beverages and great-tasting food.<br>The secret to making life better.</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Carousel items -->
                                    </div>
                                    <!--Carousel Indicator-->
                                    <ol class="carousel-indicators">
                                        <li data-target="#Carousel-929de0353e9e4d64bc7f57bd1cf391e2" data-slide-to="0" class="active"></li>
                                        <li data-target="#Carousel-929de0353e9e4d64bc7f57bd1cf391e2" data-slide-to="1" class=""></li>
                                        <li data-target="#Carousel-929de0353e9e4d64bc7f57bd1cf391e2" data-slide-to="2" class=""></li>
                                    </ol>
                                </div>
                            </div>
                        </section>
                        <script>
                            // set height for item in slider
                            jQuery(window).load(function () {
                                var screenHeight = jQuery(window).height(),
                                        screenRes = jQuery(window).width();
                                if (screenRes > 768) {
                                    jQuery('.fly-slider-full .item').css('height', screenHeight);
                                }
                            });

                            // set height for item in slider (resize image)
                            jQuery(window).resize(function () {
                                var screenHeight = jQuery(window).height(),
                                        screenRes = jQuery(window).width();
                                if (screenRes > 768) {
                                    jQuery('.fly-slider-full .item').css('height', screenHeight);
                                }
                            });

                            jQuery(document).ready(function () {
                                jQuery('#Carousel-929de0353e9e4d64bc7f57bd1cf391e2').carousel({
                                    interval: 6500});

                                if (Modernizr.touch) {
                                    jQuery('#Carousel-929de0353e9e4d64bc7f57bd1cf391e2').find('.carousel-inner').swipe({
                                        swipeLeft: function () {
                                            jQuery(this).parent().carousel('next');
                                        },
                                        swipeRight: function () {
                                            jQuery(this).parent().carousel('prev');
                                        },
                                        threshold: 30
                                    });
                                }
                            });
                        </script>
                    </div>
                </div>

                <div class="fw-row">
                    <div class="fw-col-xs-12">
                        <section class="fly-quick-nav ">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="fly-quick-nav-item col-sm-6" style="background-image: url(&#39;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/bg-1.jpg&#39;)">
                                        <i class="fly-quick-nav-icon flyicon-cutlery"></i>
                                        <h4 class="fly-quick-nav-before-title">FULFILL YOUR CRAVING</h4>
                                        <h3 class="fly-quick-nav-title">KEEP HUNGER?</h3>
                                        <a class="fly-btn fly-btn-1 fly-btn-md fly-btn-color-3 fly-btn-quick-nav" href="http://demo.flytemplates.com/flycoffee-wp/menu/" hidefocus="true" style="outline: none;"><span>More</span></a>
                                    </div>
                                    <div class="fly-quick-nav-item col-sm-6" style="background-image: url(&#39;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/bg-4.jpg&#39;)">
                                        <i class="fly-quick-nav-icon flyicon-gift"></i>
                                        <h4 class="fly-quick-nav-before-title">EVERY DAY</h4>
                                        <h3 class="fly-quick-nav-title">OFFERS JUST FOR YOU</h3>
                                        <a class="fly-btn fly-btn-1 fly-btn-md fly-btn-color-3 fly-btn-quick-nav" href="http://demo.flytemplates.com/flycoffee-wp/fly-offers/offers/" hidefocus="true" style="outline: none;"><span>More</span></a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="fw-row">
                    <div class="fw-col-xs-12">
                        <section class="fly-quick-nav ">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="fly-quick-nav-item col-sm-6" style="background-image: url(&#39;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/bg-3.jpg&#39;)">
                                        <i class="fly-quick-nav-icon flyicon-notebook"></i>
                                        <h4 class="fly-quick-nav-before-title">LATEST</h4>
                                        <h3 class="fly-quick-nav-title">NEWS AND EVENTS</h3>
                                        <a class="fly-btn fly-btn-1 fly-btn-md fly-btn-color-3 fly-btn-quick-nav" href="http://demo.flytemplates.com/flycoffee-wp/category/news/" hidefocus="true" style="outline: none;"><span>More</span></a>
                                    </div>
                                    <div class="fly-quick-nav-item col-sm-6" style="background-image: url(&#39;//demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/bg-2.jpg&#39;)">
                                        <i class="fly-quick-nav-icon flyicon-portait"></i>
                                        <h4 class="fly-quick-nav-before-title">TAKE A LOOK</h4>
                                        <h3 class="fly-quick-nav-title">AT OUR PHOTOS</h3>
                                        <a class="fly-btn fly-btn-1 fly-btn-md fly-btn-color-3 fly-btn-quick-nav" href="http://demo.flytemplates.com/flycoffee-wp/photo/" hidefocus="true" style="outline: none;"><span>More</span></a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="fw-row">
                    <div class="fw-col-xs-12">
                        <section class="fly-wrap-testimonials-slider container-min " >
                            <div class="container">
                                <div class="row">
                                    <div class="fly-divider-space space-sm"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="fly-testimonials-slider">
                                            <h5 class="fly-testimonials-slider-before-title"><span>Warm Words</span></h5>

                                            <h3 class="fly-testimonials-slider-title">FROM OUR CLIENTS</h3>

                                            <ul id="testimonials-6c18c5625ba5a58ba0f89008964f01ba">
                                                <li data-special-offers-slider="1" class="container-li">
                                                    <div class="fly-testimonials-slider-author">
                                                        <span class="fly-testimonials-slider-author-image">
                                                            <img src="http://demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/avatar-1-80x80.jpg" alt="Bryan Nylson" />
                                                        </span>
                                                        <a href="#1" class="fly-testimonials-slider-author-name">Bryan Nylson</a>
                                                    </div>
                                                    <div class="fly-testimonials-slider-content">
                                                        When arriving in town, this is one of the first stops for something interesting and amazing to eat. The menu has choices for everyone and it is the kind of place you want to come back to. I love the fish and chips there as well.								</div>
                                                </li>
                                                <li data-special-offers-slider="1" class="container-li">
                                                    <div class="fly-testimonials-slider-author">
                                                        <span class="fly-testimonials-slider-author-image">
                                                            <img src="http://demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/avatar-3-80x80.jpg" alt="Lisa Petterson" />
                                                        </span>
                                                        <a href="#2" class="fly-testimonials-slider-author-name">Lisa Petterson</a>
                                                    </div>
                                                    <div class="fly-testimonials-slider-content">
                                                        I just wanted to thank you for providing a great birthday celebration for me. Your staff did an excellent job and the food was superb. Everyone was impressed. I will recommend your expertise to my friends for an event such as this. Thanks again!								</div>
                                                </li>
                                                <li data-special-offers-slider="1" class="container-li">
                                                    <div class="fly-testimonials-slider-author">
                                                        <span class="fly-testimonials-slider-author-image">
                                                            <img src="http://demo.flytemplates.com/flycoffee-wp/wp-content/uploads/2015/07/avatar-2-80x80.jpg" alt="Thomas Jefferson" />
                                                        </span>
                                                        <a href="#3" class="fly-testimonials-slider-author-name">Thomas Jefferson</a>
                                                    </div>
                                                    <div class="fly-testimonials-slider-content">
                                                        I wanted to write to say thank you for an amazing experience and delicious dinner tonight. You made my baby daughter and I feel very welcome! I look forward to coming back. Jenny was so wonderful with my daughter too and had her laughing most of the night. Best regards, Allison								</div>
                                                </li>
                                            </ul>
                                            <div id="testimonials-6c18c5625ba5a58ba0f89008964f01ba-controls" class="fly-testimonials-slider-controls"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fly-divider-space space-sm"></div>
                                </div>
                            </div>
                            <script>
                                // Testimonials Slider
                                jQuery(document).ready(function () {
                                    setTimeout(function () {
                                        jQuery('#testimonials-6c18c5625ba5a58ba0f89008964f01ba').carouFredSel({
                                            swipe: {
                                                onTouch: true
                                            },
                                            pagination: "#testimonials-6c18c5625ba5a58ba0f89008964f01ba-controls",
                                            auto: {
                                                play: true,
                                                timeoutDuration: 10000},
                                            circular: true,
                                            infinite: true,
                                            width: '100%',
                                            scroll: {
                                                items: 1,
                                                fx: "crossfade",
                                                easing: "linear",
                                                duration: 300
                                            }
                                        });
                                    }, 0);
                                });
                            </script>
                        </section>
                    </div>
                </div>

            </div>
        </section>
    </div>
  
</div><!--#main-->
<?php get_footer(); ?>